<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'leX5vN8NQF5+x3ILE10b3sia6BFfVN+0bOgLPkXMNlbR8KBcJwYlyK7HEYtNbH7NtPXtO3AR81il7jvFukNKqw==');
define('SECURE_AUTH_KEY',  '4YnlPM7EhHs9VJSZUemIXtQqPWKFysOeFqD/wXBEPu+FZYxZPu4EVLcDtP2StbdJuLdouMMenGC5soa39542qQ==');
define('LOGGED_IN_KEY',    'iwFVxshoLnSIs3QnOZgE3hGZAedBn/UZOwzeWJx1zc71W+lBvPbgg3iPRezKegT7YCvjEEkPmyN0QZ34N09k2A==');
define('NONCE_KEY',        '0pCzxtDP1HHKDJdNLyMLabpqKkXGI29SxQZLQkDRkcPsssz/sAFI0TpCMiNimE6yhWSGf0fg3zJaqQP/Ii0MlA==');
define('AUTH_SALT',        'LYrNIxhiN03S8pURz+oHaFcyfLzTR4/QVxSJV91GS6J5DsOm/N9ntpgKdYPsWPJVuFI0ytmSAQyRLxYe5WfJ0A==');
define('SECURE_AUTH_SALT', 'eMIaeDqk2FSoyctVNidKPz5rFuKrz7Zq2NouEACvuXkWTSC9zt0G81xNqif0nQcIYIKbnEcGGEwL3CHLAusKVw==');
define('LOGGED_IN_SALT',   'sgLrdWEJ59Z2cebGDCyqPL8RyrS9j1w5P91wY/pn7vpMDhqBfrmw2+fdtCsUyjpfdX+VvZhdWfb+IzQ5jg9j7Q==');
define('NONCE_SALT',       'xnHTbAdt5Fzc7Xt9iEhjUukk0H+9fhQzf8v1Dy0NXOO2RbeLkILZXhgo9wPriz+0jnwmzcJUIUWL158KGQ384A==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
